-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2018 at 02:53 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dca`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_admin_detail` (IN `id_admin` INT(11))  NO SQL
BEGIN
SELECT * FROM t_admin WHERE admin_id=id_admin;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_admin_tambah` (IN `id_admin` INT(11), IN `nama_admin` VARCHAR(50), IN `kata_sandi` VARCHAR(300))  NO SQL
BEGIN
INSERT INTO t_admin
		(admin_id, admin_nama, admin_kata_sandi)
        VALUES
		(id_admin, nama_admin, kata_sandi);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_admin_tampil` ()  NO SQL
BEGIN
SELECT * FROM t_admin;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_admin_detail` (IN `nama_admin` VARCHAR(50))  NO SQL
BEGIN
SELECT * FROM t_admin WHERE admin_nama=nama_admin;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_auditor_detail` (IN `nip_auditor` VARCHAR(18))  NO SQL
BEGIN
SELECT * FROM t_auditor auditor JOIN t_pangkat pangkat ON auditor.auditor_nip=pangkat.auditor_nip WHERE auditor.auditor_nip=nip_auditor;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_auditor_tampil` ()  NO SQL
BEGIN
SELECT * FROM t_auditor;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_jabatan_detail` (IN `nip_auditor` VARCHAR(18))  NO SQL
BEGIN
SELECT
	jabatan.auditor_nip,
    jfa.jfa_nama,
    jenjang_jfa.jenjang_jfa_nama,
    jabatan.jabatan_no_surat_keputusan,
    jabatan.jabatan_tgl_surat_keputusan,
    jabatan.jabatan_tgl_mulai_tugas,
    jabatan.jabatan_sk

FROM t_jabatan jabatan

JOIN r_jfa jfa
ON jabatan.jfa_id=jfa.jfa_id
JOIN r_jenjang_jfa jenjang_jfa
ON jabatan.jenjang_jfa_id=jenjang_jfa.jenjang_jfa_id

WHERE auditor_nip=nip_auditor;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_pangkat_detail` (IN `nip_auditor` VARCHAR(18))  NO SQL
BEGIN
SELECT 
	pangkat.auditor_nip, 
    golongan.golongan_ruang_nama, 
    golongan.golongan_ruang_deskripsi, 
    pangkat.pangkat_no_surat_keputusan, 
    pangkat.pangkat_tgl_surat_keputusan, 
    pangkat.pangkat_tgl_mulai_tugas, 
    pangkat.pangkat_sk 
FROM t_pangkat pangkat

JOIN r_golongan_ruang golongan
ON pangkat.golongan_ruang_id=golongan.golongan_ruang_id

WHERE auditor_nip=nip_auditor;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_pendidikan_detail` (IN `nip_auditor` VARCHAR(18))  NO SQL
BEGIN
SELECT
	auditor_nip,
    pendidikan_tingkat,
    pendidikan_lembaga,
    pendidikan_jurusan,
    pendidikan_ijazah
    
FROM t_pendidikan 

WHERE 
	auditor_nip=nip_auditor 
    AND 
    pendidikan_tahun_lulus IS NOT NULL 
ORDER BY pendidikan_tahun_lulus DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_pendidikan_tampil` (IN `nip_auditor` VARCHAR(18))  NO SQL
BEGIN
SELECT
	auditor_nip,
    pendidikan_tingkat,
    pendidikan_lembaga,
    pendidikan_jurusan,
    pendidikan_ijazah
    
FROM t_pendidikan 

WHERE 
	auditor_nip=nip_auditor 
ORDER BY pendidikan_tahun_masuk DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_api_sertifikasi_jfa_detail` (IN `nip_auditor` VARCHAR(18))  NO SQL
BEGIN
SELECT 
	auditor_nip,
    sertifikasi_sertifikat

FROM t_sertifikasi_jfa

WHERE auditor_nip=nip_auditor;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `r_golongan_ruang`
--

CREATE TABLE `r_golongan_ruang` (
  `golongan_ruang_id` tinyint(3) NOT NULL COMMENT 'id primary key golongan ruang',
  `golongan_ruang_nama` varchar(50) NOT NULL COMMENT 'nama golongan ruang',
  `golongan_ruang_deskripsi` varchar(50) DEFAULT NULL COMMENT 'Deskripsi golongan ruang',
  `golongan_ruang_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna)',
  `golongan_ruang_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `golongan_ruang_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna)',
  `golongan_ruang_tgl_diubah` date DEFAULT NULL COMMENT 'Tanggal diiubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table referensi golongan';

--
-- Dumping data for table `r_golongan_ruang`
--

INSERT INTO `r_golongan_ruang` (`golongan_ruang_id`, `golongan_ruang_nama`, `golongan_ruang_deskripsi`, `golongan_ruang_dibuat_oleh`, `golongan_ruang_tgl_dibuat`, `golongan_ruang_diubah_oleh`, `golongan_ruang_tgl_diubah`) VALUES
(1, 'Golongan Ruang II/C', 'Pengatur, Golongan Ruang II/C', 1, '2018-01-20', 0, '0000-00-00'),
(2, 'Golongan Ruang II/D', 'Pengatur TIngkat I, Golongan Ruang II/D', 1, '2018-01-20', 0, '0000-00-00'),
(3, 'Golongan Ruang III/A', 'Penata Muda, Golongan Ruang III/A', 1, '2018-01-20', 0, '0000-00-00'),
(4, 'Golongan Ruang III/B', 'Penata Muda Tingkat I, Golongan Ruang III/B', 1, '2018-01-20', 0, '0000-00-00'),
(5, 'Golongan Ruang III/C', 'Penata, Golongan Ruang III/C', 1, '2018-01-20', 0, '0000-00-00'),
(6, 'Golongan Ruang III/D', 'Penata Tingkat I, Golongan Ruang III/D', 1, '2018-01-20', 0, '0000-00-00'),
(7, 'Golongan Ruang IV/A', 'Pembina, Golongan Ruang IV/A', 1, '2018-01-20', 0, '0000-00-00'),
(8, 'Golongan Ruang IV/B', 'Pembina Tingkat I, Golongan Ruang IV/B', 1, '2018-01-20', 0, '0000-00-00'),
(9, 'Golongan Ruang IV/C', 'Pembina Utama Muda, Golongan Ruang IV/C', 1, '2018-01-20', 0, '0000-00-00'),
(10, 'Golongan Ruang IV/D', 'Pembina Utama Madya, Golongan Ruang IV/D', 1, '2018-01-20', 0, '0000-00-00'),
(11, 'Golongan Ruang IV/E', 'Pembina Utama, Golongan Ruang IV/E', 1, '2018-01-20', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `r_jenjang_jfa`
--

CREATE TABLE `r_jenjang_jfa` (
  `jenjang_jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key jenjang jfa',
  `jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key jfa auditor',
  `jenjang_jfa_nama` varchar(50) NOT NULL COMMENT 'nama jenjang jabatan fungsional auditor',
  `jenjang_jfa_deskripsi` varchar(500) DEFAULT NULL COMMENT 'Deskripsi jenjang jabatan fungsional auditor',
  `jenjang_jfa_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna)',
  `jenjang_jfa_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `jenjang_jfa_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna_',
  `jenjang_jfa_tgl_diubah` date DEFAULT NULL COMMENT 'tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table referensi jenjang jfa';

--
-- Dumping data for table `r_jenjang_jfa`
--

INSERT INTO `r_jenjang_jfa` (`jenjang_jfa_id`, `jfa_id`, `jenjang_jfa_nama`, `jenjang_jfa_deskripsi`, `jenjang_jfa_dibuat_oleh`, `jenjang_jfa_tgl_dibuat`, `jenjang_jfa_diubah_oleh`, `jenjang_jfa_tgl_diubah`) VALUES
(1, 1, 'Auditor Pelaksana', 'Pengatu dan Pengatur Tingkat I', 0, '0000-00-00', 0, '0000-00-00'),
(3, 2, 'Auditor Pelaksana Lanjutan', 'Penata Muda dan Penata Muda Tingkat I', 1, '2017-12-30', 2, '2018-01-10');

-- --------------------------------------------------------

--
-- Table structure for table `r_jfa`
--

CREATE TABLE `r_jfa` (
  `jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key jfa',
  `jfa_nama` varchar(50) NOT NULL COMMENT 'Nama jabatan fungsional auditor',
  `jfa_deskripsi` varchar(500) DEFAULT NULL COMMENT 'Deskripsi jabatan fungsional auditor',
  `jfa_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomor induk pegawai audiitor (sebagai pengguna_',
  `jfa_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `jfa_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomor induk pegawai auditor(sebagai pengguna)',
  `jfa_tgl_diubah` date DEFAULT NULL COMMENT 'Tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table referensi jfa';

--
-- Dumping data for table `r_jfa`
--

INSERT INTO `r_jfa` (`jfa_id`, `jfa_nama`, `jfa_deskripsi`, `jfa_dibuat_oleh`, `jfa_tgl_dibuat`, `jfa_diubah_oleh`, `jfa_tgl_diubah`) VALUES
(1, 'Auditor Terampil', 'Jejang Jabatan Fungsional Auditor Terampil paling rendah hingga paling tinggi: Auditor Pelaksana; Auditor Pelaksana Lanjutan; dan Auditor Penyelia.', 0, '2018-01-15', 0, '0000-00-00'),
(2, 'Auditor Muda', 'Jenjang Jabatan Fungsional Auditor Paling Rendah', 1, '2018-01-15', 2, '2018-01-17');

-- --------------------------------------------------------

--
-- Table structure for table `r_pangkat_jabatan_auditor`
--

CREATE TABLE `r_pangkat_jabatan_auditor` (
  `pangkat_jabatan_jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key pangkat dan jabatan auditor',
  `jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key jfa auditor',
  `jenjang_jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key jenjang jfa',
  `golongan_ruang_id` tinyint(3) NOT NULL COMMENT 'id primary key golongan ruang auditor',
  `pangkat_jabatan_jfa_nama` varchar(50) NOT NULL COMMENT 'nama pangkat dan jabatan jabatan fungsional auditor',
  `pangkat_jabatan_jfa_deskripsi` varchar(500) DEFAULT NULL COMMENT 'Deskripsi pangkat dan jabatan jabatan fungsional auditor',
  `pangkat_jabatan_jfa_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna)',
  `pangkat_jabatan_jfa_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `pangkat_jabatan_jfa_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna_',
  `pangkat_jabatan_jfa_tgl_diubah` date DEFAULT NULL COMMENT 'tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table referensi pangkat dan jabatan auditor';

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_nama` varchar(50) NOT NULL,
  `admin_kata_sandi` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`admin_id`, `admin_nama`, `admin_kata_sandi`) VALUES
(1, 'dummy', '$2y$10$UYYX3hIyMoZUpIB4wKlh/.ve4MxTXEvJZRdp4i8wyAH/LD64FtRn.'),
(5, 'testing', '$2y$10$Cz/DnzSmRWeZRvi.QOsK7O/NDXiL2hZStNPnO45UUMvDn7yM7mzWG');

-- --------------------------------------------------------

--
-- Table structure for table `t_auditor`
--

CREATE TABLE `t_auditor` (
  `auditor_id` int(11) NOT NULL COMMENT 'id primary key auditor',
  `auditor_nip` varchar(18) NOT NULL COMMENT 'Nomor Induk Pegawai Auditor',
  `auditor_nama_lengkap` varchar(50) NOT NULL COMMENT 'Nama lengkap',
  `auditor_gelar_depan` char(15) DEFAULT NULL COMMENT 'Gelar di depan nama',
  `auditor_gelar_belakang` char(15) DEFAULT NULL COMMENT 'Gelar di belakang nama',
  `auditor_tempat_lahir` varchar(25) NOT NULL COMMENT 'Tempat kelahiran',
  `auditor_tgl_lahir` date NOT NULL COMMENT 'Tanggal kelahiran',
  `auditor_no_hp` varchar(15) NOT NULL COMMENT 'Kontak telepon genggam auditor',
  `auditor_surel` varchar(30) NOT NULL COMMENT 'Surat elektronik',
  `auditor_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomer induk pegawai auditor (sebagai pengguna)',
  `auditor_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `auditor_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomer induk pegawai auditor (sebagai pengguna_',
  `auditor_tgl_diubah` date DEFAULT NULL COMMENT 'Tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table data auditor';

--
-- Dumping data for table `t_auditor`
--

INSERT INTO `t_auditor` (`auditor_id`, `auditor_nip`, `auditor_nama_lengkap`, `auditor_gelar_depan`, `auditor_gelar_belakang`, `auditor_tempat_lahir`, `auditor_tgl_lahir`, `auditor_no_hp`, `auditor_surel`, `auditor_dibuat_oleh`, `auditor_tgl_dibuat`, `auditor_diubah_oleh`, `auditor_tgl_diubah`) VALUES
(2, '123456789012345678', 'Ardianto DJ', 'Prof.', 'S,E', 'Kepahiang', '1989-08-10', '08575461466', 'dianto.dj@gmail.com', 1, '2017-12-30', 2, '2018-01-20'),
(3, '876543210987654321', 'Nama Lengkap', 'Dr.', 'M.Kom', 'Jakarta', '1980-12-12', '088812', 'nama.lengkap@mail.com', 1, '2018-01-26', 2, '2018-01-27');

-- --------------------------------------------------------

--
-- Table structure for table `t_jabatan`
--

CREATE TABLE `t_jabatan` (
  `jabatan_id` int(11) NOT NULL COMMENT 'id primary key jabatan',
  `auditor_nip` varchar(18) NOT NULL COMMENT 'nip uniqe auditor',
  `jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key table ref jf',
  `jenjang_jfa_id` tinyint(3) NOT NULL COMMENT 'id primary key table ref jenjang jfa',
  `jabatan_no_surat_keputusan` varchar(50) NOT NULL COMMENT 'Nomor surat keputusan',
  `jabatan_tgl_surat_keputusan` date NOT NULL COMMENT 'Tanggal surat keputusan dibuat',
  `jabatan_tgl_mulai_tugas` date NOT NULL COMMENT 'TMT pada saat menjabat',
  `jabatan_sk` varchar(128) NOT NULL COMMENT 'URL upload file surat keputusan',
  `jabatan_dibuat_oleh` int(25) NOT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna_',
  `jabatan_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `jabatan_diubah_oleh` int(25) DEFAULT NULL COMMENT 'Nomer induk pegawai auditor (sebagai pengguna)',
  `Jabatan_tgl_diubah` date DEFAULT NULL COMMENT 'Tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table jabatan auditor';

--
-- Dumping data for table `t_jabatan`
--

INSERT INTO `t_jabatan` (`jabatan_id`, `auditor_nip`, `jfa_id`, `jenjang_jfa_id`, `jabatan_no_surat_keputusan`, `jabatan_tgl_surat_keputusan`, `jabatan_tgl_mulai_tugas`, `jabatan_sk`, `jabatan_dibuat_oleh`, `jabatan_tgl_dibuat`, `jabatan_diubah_oleh`, `Jabatan_tgl_diubah`) VALUES
(6, '123456789012345678', 2, 3, 'SKJ/823/BPKP.514/2018', '2018-01-22', '2018-02-01', 'https://datacenterauditor.io/file/jabatan/123456789012345678-skj-01.pdf', 1, '2018-01-20', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `t_pangkat`
--

CREATE TABLE `t_pangkat` (
  `pangkat_id` int(11) NOT NULL COMMENT 'id primary key pangkat',
  `auditor_nip` varchar(18) NOT NULL COMMENT 'nip primary key auditor',
  `golongan_ruang_id` tinyint(3) NOT NULL COMMENT 'id primary key table ref golongan ruang',
  `pangkat_no_surat_keputusan` varchar(50) NOT NULL COMMENT 'Nomor surat keputusan',
  `pangkat_tgl_surat_keputusan` date NOT NULL COMMENT 'Tanggal surat keputusan dibuat',
  `pangkat_tgl_mulai_tugas` date NOT NULL COMMENT 'TMT pada saat pengangkatan',
  `pangkat_sk` varchar(128) NOT NULL,
  `pangkat_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna_',
  `pangkat_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `pangkat_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomor induk pegawai auditor (sebagai pengguna)',
  `pangkat_tgl_diubah` date DEFAULT NULL COMMENT 'Tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pangkat`
--

INSERT INTO `t_pangkat` (`pangkat_id`, `auditor_nip`, `golongan_ruang_id`, `pangkat_no_surat_keputusan`, `pangkat_tgl_surat_keputusan`, `pangkat_tgl_mulai_tugas`, `pangkat_sk`, `pangkat_dibuat_oleh`, `pangkat_tgl_dibuat`, `pangkat_diubah_oleh`, `pangkat_tgl_diubah`) VALUES
(3, '123456789012345678', 3, 'SK/823/BPKP.514/2018', '2018-01-22', '2018-02-01', 'https://datacenterauditor.io/file/pangkat/123456789012345678-skp-09.pdf', 1, '2018-01-22', NULL, NULL),
(4, '876543210987654321', 1, 'SK/823/BPKP.515/2018', '2018-01-18', '2018-02-01', '', 1, '2018-01-18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_pendidikan`
--

CREATE TABLE `t_pendidikan` (
  `pendidikan_id` bigint(11) NOT NULL COMMENT 'id primary key pendidikan',
  `auditor_nip` varchar(18) NOT NULL COMMENT 'id primary key auditor',
  `pendidikan_tingkat` char(25) NOT NULL COMMENT 'tingkat pendidikan',
  `pendidikan_lembaga` varchar(50) NOT NULL COMMENT 'Nama Lembaga',
  `pendidikan_jurusan` varchar(50) NOT NULL COMMENT 'Nama jurusan',
  `pendidikan_tahun_masuk` year(4) NOT NULL COMMENT 'Tahun masuk perkuliahan',
  `pendidikan_tahun_lulus` year(4) DEFAULT NULL COMMENT 'Tahun lulus / wisuda perkuliahan',
  `pendidikan_ijazah` varchar(128) DEFAULT NULL,
  `pendidikan_dibuat_oleh` bigint(25) NOT NULL COMMENT 'Nomer induk pegawai auditor (sebagai pengguna',
  `pendidikan_tgl_dibuat` date NOT NULL COMMENT 'Tanggal dibuat',
  `pendidikan_diubah_oleh` bigint(25) DEFAULT NULL COMMENT 'Nomer induk pegawai auditor (sebagai pengguna)',
  `pendidikan_tgl_diubah` date DEFAULT NULL COMMENT 'Tanggal diubah'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table pendidikan auditor';

--
-- Dumping data for table `t_pendidikan`
--

INSERT INTO `t_pendidikan` (`pendidikan_id`, `auditor_nip`, `pendidikan_tingkat`, `pendidikan_lembaga`, `pendidikan_jurusan`, `pendidikan_tahun_masuk`, `pendidikan_tahun_lulus`, `pendidikan_ijazah`, `pendidikan_dibuat_oleh`, `pendidikan_tgl_dibuat`, `pendidikan_diubah_oleh`, `pendidikan_tgl_diubah`) VALUES
(3, '123456789012345678', 'Strata 3', 'Universitas Indonesia', 'Sistem Informasi', 2013, 2015, 'https://datacenterauditor.io/file/ijazah/123456789012345678-s3-05.pdf', 1, '2018-01-20', 0, '0000-00-00'),
(4, '123456789012345678', 'Strata 2', 'Universitas Gunadarma', 'Sistem Informasi', 2009, 2013, 'https://datacenterauditor.io/file/ijazah/123456789012345678-s3-04.pdf', 1, '2018-01-20', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `t_sertifikasi_jfa`
--

CREATE TABLE `t_sertifikasi_jfa` (
  `sertifikasi_jfa_id` int(11) NOT NULL,
  `auditor_nip` varchar(18) NOT NULL,
  `sertifikasi_sertifikat` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_sertifikasi_jfa`
--

INSERT INTO `t_sertifikasi_jfa` (`sertifikasi_jfa_id`, `auditor_nip`, `sertifikasi_sertifikat`) VALUES
(1, '123456789012345678', 'https://datacenterauditor.io/file/sertifikasi/123456789012345678-08.pdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `r_golongan_ruang`
--
ALTER TABLE `r_golongan_ruang`
  ADD PRIMARY KEY (`golongan_ruang_id`);

--
-- Indexes for table `r_jenjang_jfa`
--
ALTER TABLE `r_jenjang_jfa`
  ADD PRIMARY KEY (`jenjang_jfa_id`),
  ADD KEY `jfa_id` (`jfa_id`);

--
-- Indexes for table `r_jfa`
--
ALTER TABLE `r_jfa`
  ADD PRIMARY KEY (`jfa_id`);

--
-- Indexes for table `r_pangkat_jabatan_auditor`
--
ALTER TABLE `r_pangkat_jabatan_auditor`
  ADD PRIMARY KEY (`pangkat_jabatan_jfa_id`),
  ADD KEY `jfa_id` (`jfa_id`),
  ADD KEY `jenjang_jfa_id` (`jenjang_jfa_id`),
  ADD KEY `golongan_ruang_id` (`golongan_ruang_id`);

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `t_auditor`
--
ALTER TABLE `t_auditor`
  ADD PRIMARY KEY (`auditor_id`),
  ADD UNIQUE KEY `auditor_nip` (`auditor_nip`);

--
-- Indexes for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  ADD PRIMARY KEY (`jabatan_id`),
  ADD KEY `jenjang_jfa_id` (`jenjang_jfa_id`),
  ADD KEY `jfa_id` (`jfa_id`),
  ADD KEY `auditor_id` (`auditor_nip`) USING BTREE;

--
-- Indexes for table `t_pangkat`
--
ALTER TABLE `t_pangkat`
  ADD PRIMARY KEY (`pangkat_id`),
  ADD KEY `pangkat_golongan_ruang` (`golongan_ruang_id`) USING BTREE,
  ADD KEY `pangkat_auditor_id` (`auditor_nip`) USING BTREE;

--
-- Indexes for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  ADD PRIMARY KEY (`pendidikan_id`),
  ADD KEY `pendidikan_auditor_nip` (`auditor_nip`) USING BTREE;

--
-- Indexes for table `t_sertifikasi_jfa`
--
ALTER TABLE `t_sertifikasi_jfa`
  ADD PRIMARY KEY (`sertifikasi_jfa_id`),
  ADD KEY `auditor_nip` (`auditor_nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `r_golongan_ruang`
--
ALTER TABLE `r_golongan_ruang`
  MODIFY `golongan_ruang_id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'id primary key golongan ruang', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `r_jenjang_jfa`
--
ALTER TABLE `r_jenjang_jfa`
  MODIFY `jenjang_jfa_id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'id primary key jenjang jfa', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `r_jfa`
--
ALTER TABLE `r_jfa`
  MODIFY `jfa_id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'id primary key jfa', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `r_pangkat_jabatan_auditor`
--
ALTER TABLE `r_pangkat_jabatan_auditor`
  MODIFY `pangkat_jabatan_jfa_id` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'id primary key pangkat dan jabatan auditor';

--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_auditor`
--
ALTER TABLE `t_auditor`
  MODIFY `auditor_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id primary key auditor', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  MODIFY `jabatan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id primary key jabatan', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_pangkat`
--
ALTER TABLE `t_pangkat`
  MODIFY `pangkat_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id primary key pangkat', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  MODIFY `pendidikan_id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id primary key pendidikan', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_sertifikasi_jfa`
--
ALTER TABLE `t_sertifikasi_jfa`
  MODIFY `sertifikasi_jfa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `r_jenjang_jfa`
--
ALTER TABLE `r_jenjang_jfa`
  ADD CONSTRAINT `r_jenjang_jfa_ibfk_1` FOREIGN KEY (`jfa_id`) REFERENCES `r_jfa` (`jfa_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `r_pangkat_jabatan_auditor`
--
ALTER TABLE `r_pangkat_jabatan_auditor`
  ADD CONSTRAINT `r_pangkat_jabatan_auditor_ibfk_1` FOREIGN KEY (`golongan_ruang_id`) REFERENCES `r_golongan_ruang` (`golongan_ruang_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `r_pangkat_jabatan_auditor_ibfk_2` FOREIGN KEY (`jenjang_jfa_id`) REFERENCES `r_jenjang_jfa` (`jenjang_jfa_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `r_pangkat_jabatan_auditor_ibfk_3` FOREIGN KEY (`jfa_id`) REFERENCES `r_jfa` (`jfa_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  ADD CONSTRAINT `t_jabatan_ibfk_1` FOREIGN KEY (`jfa_id`) REFERENCES `r_jfa` (`jfa_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `t_jabatan_ibfk_2` FOREIGN KEY (`jenjang_jfa_id`) REFERENCES `r_jenjang_jfa` (`jenjang_jfa_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `t_jabatan_ibfk_3` FOREIGN KEY (`auditor_nip`) REFERENCES `t_auditor` (`auditor_nip`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `t_pangkat`
--
ALTER TABLE `t_pangkat`
  ADD CONSTRAINT `t_pangkat_ibfk_2` FOREIGN KEY (`golongan_ruang_id`) REFERENCES `r_golongan_ruang` (`golongan_ruang_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pangkat_ibfk_3` FOREIGN KEY (`auditor_nip`) REFERENCES `t_auditor` (`auditor_nip`);

--
-- Constraints for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  ADD CONSTRAINT `t_pendidikan_ibfk_1` FOREIGN KEY (`auditor_nip`) REFERENCES `t_auditor` (`auditor_nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_sertifikasi_jfa`
--
ALTER TABLE `t_sertifikasi_jfa`
  ADD CONSTRAINT `t_sertifikasi_jfa_ibfk_1` FOREIGN KEY (`auditor_nip`) REFERENCES `t_auditor` (`auditor_nip`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
