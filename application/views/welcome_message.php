<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #00a2cb;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 28px;
		font-weight: 1px;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	h2 {
		color: #00a2cb;
		background-color: transparent;
		/* border-bottom: 1px solid #D0D0D0; */
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		/* padding: 14px 15px 10px 15px; */
	}

	h3 {
		color: #00a2cb;
		background-color: transparent;
		/* border-bottom: 1px solid #D0D0D0; */
		font-size: 14px;
		font-weight: normal;
		margin: 0 0 0 0;
		/* padding: 14px 15px 10px 15px; */
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 6px 10px 6px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Data Center API</h1>

	<div id="body">

		<h3>Pengembangan API menggunakan komponen berikut :</h3>

		<code>
			<p>- CodeIgniter <?php echo (ENVIRONMENT === 'development') ? '<strong>' . CI_VERSION . '</strong>' : '' ?></p>
			<p>- PHP <strong>7.1.9</strong></p>
			<p>- MySql <strong>5.0.12</strong></p>
			<p>- Rest Controller <strong>3.0.0</strong></p>
			<p>- PHP-JWT <strong>4.0.0</strong></p>
			<p>- JSON <strong>1.5.0</strong></p>
			<p>- Postman <strong>5.5.0</strong></p>
		</code>
	</div>

	<p class="footer">Data Center API Versi 0.6.4 - <a href="Dokumentasi_Data_Center_API.pdf">Dokumentasi API</a>.</p>

</div>

</body>
</html>
