<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| URI API ROUTING
| -------------------------------------------------------------------------
*/

/*
| -------------------------------------------------------------------------
| API MENAMBAHKAN ADMIN
| -------------------------------------------------------------------------
*/
$route['api/admin']['POST']                           = 'api/apiadmincontroller/admin';

/*
| -------------------------------------------------------------------------
| API TOKEN API
| -------------------------------------------------------------------------
*/
$route['api/viewtoken']['POST']                       = 'api/restdata/viewtoken';

/*
| -------------------------------------------------------------------------
| API DATA UMUM AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/auditor']['GET']                          = 'api/apiauditorcontroller/auditor';
$route['api/auditor/format/(:any)']['GET']            = 'api/apiauditorcontroller/auditor/format/$1';
$route['api/auditor/(:num)']['GET']                   = 'api/apiauditorcontroller/auditor/id/$1';
$route['api/auditor/(:num)/format/(:any)']['GET']     = 'api/apiauditorcontroller/auditor/id/$1/format/$2';

$route['api/auditor']['POST']                         = 'api/apiauditorcontroller/auditor';
$route['api/nip']['POST']                             = 'api/apiauditorcontroller/nip';
$route['api/auditor/(:num)']['PUT']                   = 'api/apiauditorcontroller/auditor/id/$1';
$route['api/auditor/(:num)']['DELETE']                = 'api/apiauditorcontroller/auditor/id/$1';

/*
| -------------------------------------------------------------------------
| API PENDIDIKAN AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/pendidikan']['GET']                          = 'api/apipendidikancontroller/pendidikan';
$route['api/pendidikan/format/(:any)']['GET']            = 'api/apipendidikancontroller/pendidikan/format/$1';
$route['api/pendidikan/(:num)']['GET']                   = 'api/apipendidikancontroller/pendidikan/id/$1';
$route['api/pendidikan/(:num)/format/(:any)']['GET']     = 'api/apipendidikancontroller/pendidikan/id/$1/format/$2';

$route['api/pendidikan']['POST']                         = 'api/apipendidikancontroller/pendidikan';
$route['api/pendidikan/(:num)']['PUT']                   = 'api/apipendidikancontroller/pendidikan/id/$1';
$route['api/pendidikan/(:num)']['DELETE']                = 'api/apipendidikancontroller/pendidikan/id/$1';

/*
| -------------------------------------------------------------------------
| API DIKLAT AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/diklat']['GET']                          = 'api/apidiklatcontroller/diklat';
$route['api/diklat/format/(:any)']['GET']            = 'api/apidiklatcontroller/diklat/format/$1';
$route['api/diklat/(:num)']['GET']                   = 'api/apidiklatcontroller/diklat/id/$1';
$route['api/diklat/(:num)/format/(:any)']['GET']     = 'api/apidiklatcontroller/diklat/id/$1/format/$2';

$route['api/diklat']['POST']                         = 'api/apidiklatcontroller/diklat';
$route['api/diklat/(:num)']['PUT']                   = 'api/apidiklatcontroller/diklat/id/$1';
$route['api/diklat/(:num)']['DELETE']                = 'api/apidiklatcontroller/diklat/id/$1';

/*
| -------------------------------------------------------------------------
| API GOLONGAN/PANGKAT AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/pangkat']['GET']                          = 'api/apipangkatcontroller/pangkat';
$route['api/pangkat/format/(:any)']['GET']            = 'api/apipangkatcontroller/pangkat/format/$1';
$route['api/pangkat/(:num)']['GET']                   = 'api/apipangkatcontroller/pangkat/id/$1';
$route['api/pangkat/(:num)/format/(:any)']['GET']     = 'api/apipangkatcontroller/pangkat/id/$1/format/$2';

$route['api/pangkat']['POST']                         = 'api/apipangkatcontroller/pangkat';
$route['api/pangkat/(:num)']['PUT']                   = 'api/apipangkatcontroller/pangkat/id/$1';
$route['api/pangkat/(:num)']['DELETE']                = 'api/apipangkatcontroller/pangkat/id/$1';

/*
| -------------------------------------------------------------------------
| API JABATAN AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/jabatan']['GET']                          = 'api/apijabatancontroller/jabatan';
$route['api/jabatan/format/(:any)']['GET']            = 'api/apijabatancontroller/jabatan/format/$1';
$route['api/jabatan/(:num)']['GET']                   = 'api/apijabatancontroller/jabatan/id/$1';
$route['api/jabatan/(:num)/format/(:any)']['GET']     = 'api/apijabatancontroller/jabatan/id/$1/format/$2';

$route['api/jabatan']['POST']                         = 'api/apijabatancontroller/jabatan';
$route['api/jabatan/(:num)']['PUT']                   = 'api/apijabatancontroller/jabatan/id/$1';
$route['api/jabatan/(:num)']['DELETE']                = 'api/apijabatancontroller/jabatan/id/$1';

/*
| -------------------------------------------------------------------------
| API PENILAIAN ANGKA KREDIT AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/angkakredit']['GET']                          = 'api/apiangkakreditcontroller/angkakredit';
$route['api/angkakredit/format/(:any)']['GET']            = 'api/apiangkakreditcontroller/angkakredit/format/$1';
$route['api/angkakredit/(:num)']['GET']                   = 'api/apiangkakreditcontroller/angkakredit/id/$1';
$route['api/angkakredit/(:num)/format/(:any)']['GET']     = 'api/apiangkakreditcontroller/angkakredit/id/$1/format/$2';

$route['api/angkakredit']['POST']                         = 'api/apiangkakreditcontroller/angkakredit';
$route['api/angkakredit/(:num)']['PUT']                   = 'api/apiangkakreditcontroller/angkakredit/id/$1';
$route['api/angkakredit/(:num)']['DELETE']                = 'api/apiangkakreditcontroller/angkakredit/id/$1';

/*
| -------------------------------------------------------------------------
| API SERTIFIKASI JFA AUDITOR
| -------------------------------------------------------------------------
*/
$route['api/sertifikasi']['GET']                          = 'api/apisertifikasicontroller/sertifikasi';
$route['api/sertifikasi/format/(:any)']['GET']            = 'api/apisertifikasicontroller/sertifikasi/format/$1';
$route['api/sertifikasi/(:num)']['GET']                   = 'api/apisertifikasicontroller/sertifikasi/id/$1';
$route['api/sertifikasi/(:num)/format/(:any)']['GET']     = 'api/apisertifikasicontroller/sertifikasi/id/$1/format/$2';

$route['api/sertifikasi']['POST']                         = 'api/apisertifikasicontroller/sertifikasi';
$route['api/sertifikasi/(:num)']['PUT']                   = 'api/apisertifikasicontroller/sertifikasi/id/$1';
$route['api/sertifikasi/(:num)']['DELETE']                = 'api/apisertifikasicontroller/sertifikasi/id/$1';
