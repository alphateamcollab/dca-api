<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mymodel extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function insertadmin($nama,$kata_sandi)
  {
    $this->db->query("call sp_admin_tambah('".NULL."','".$nama."','".$kata_sandi."')");
  }

/*
|--------------------------------------------------------------------------
| Data Auditor, tabel t_auditor
|--------------------------------------------------------------------------
*/

  public function selectauditor()
  {
    $query=$this->db->query("call sp_api_auditor_tampil()");
	    mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0) {
              // hasil sebagai array [{hasil_1},{hasil_2},{hasil_n}]
                return $query->result_array();
            }
  }

  public function selectauditorwhere($id)
  {
    $query=$this->db->query("call sp_api_auditor_detail('".$id."')");
	    mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()==1) {
                return $query->row_array();
            }
  }

/*
|--------------------------------------------------------------------------
| Data Pendidikan Auditor, tabel t_pendidikan
|--------------------------------------------------------------------------
*/

  public function selectpendidikan($id)
  {
    $query=$this->db->query("call sp_api_pendidikan_tampil('".$id."')");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0) {
              // hasil sebagai array [{hasil_1},{hasil_2},{hasil_n}]
                return $query->result_array();
            }
  }

  public function selectpendidikanwhere($id)
  {
    $query=$this->db->query("call sp_api_pendidikan_detail('".$id."')");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0) {
                return $query->row_array();
            }
  }

/*
|--------------------------------------------------------------------------
| Data Pangkat Auditor, tabel t_pangkat
|--------------------------------------------------------------------------
*/

  public function selectpangkat()
  {
    $query=$this->db->query("call sp_api_pangkat_tampil()");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0) {
              // hasil sebagai array [{hasil_1},{hasil_2},{hasil_n}]
                return $query->result_array();
            }
  }

  public function selectpangkatwhere($id)
  {
    $query=$this->db->query("call sp_api_pangkat_detail('".$id."')");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()==1) {
                return $query->row_array();
            }
  }

/*
|--------------------------------------------------------------------------
| Data Jabatan Auditor, tabel t_jabatan
|--------------------------------------------------------------------------
*/

  public function selectjabatan()
  {
    $query=$this->db->query("call sp_api_jabatan_tampil()");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0)
            {
              // hasil sebagai array [{hasil_1},{hasil_2},{hasil_n}]
                return $query->result_array();
            }
  }

  public function selectjabatanwhere($id)
  {
    $query=$this->db->query("call sp_api_jabatan_detail('".$id."')");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()==1)
            {
                return $query->row_array();
            }
  }

/*
|--------------------------------------------------------------------------
| Data Sertifikasi JFA (Jabatan Fungsional Auditor), tabel t_sertifikasi_jfa
|--------------------------------------------------------------------------
*/

  public function selectsertifikasi()
  {
    $query=$this->db->query("call sp_api_sertifikasi_jfa_tampil()");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0)
            {
              // hasil sebagai array [{hasil_1},{hasil_2},{hasil_n}]
                return $query->result_array();
            }
  }

  public function selectsertifikasiwhere($id)
  {
    $query=$this->db->query("call sp_api_sertifikasi_jfa_detail('".$id."')");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()==1){
                return $query->row_array();
            }
  }

/*
|--------------------------------------------------------------------------
| Data PAK (Penilaian Angka Kredit) Auditor, tabel t_pak
|--------------------------------------------------------------------------
*/

  public function selectpak()
  {
    $query=$this->db->query("call sp_api_pak_tampil()");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()>0)
            {
              // hasil sebagai array [{hasil_1},{hasil_2},{hasil_n}]
                return $query->result_array();
            }
  }

  public function selectpakwhere($id)
  {
    $query=$this->db->query("call sp_api_pak_detail_nip('".$id."')");
      mysqli_next_result( $this->db->conn_id);
            if($query->num_rows()==1){
                return $query->row_array();
            }
  }

}
