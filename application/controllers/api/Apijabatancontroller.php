<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/api/Restdata.php';

/**
 *  class untuk Api jabatan, load Mymodel, cektoken di controller Restdata
 */
class Apijabatancontroller extends Restdata
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('mymodel');
    //mengecek token pada class Restdata, di mana jika token invalid maka akan melakukan exit
    $this->cektoken();
  }

  function jabatan_get()
  {
    $id = $this->get('id',TRUE);
    //jika user menambahkan id (nip) maka akan di select berdasarkan id (nip), jika tidak maka akan di select seluruhnya
    $data = ($id) ? $this->mymodel->selectjabatanwhere($id) : $this->mymodel->selectjabatan();

    if ($data!=FALSE) {
      //mengembalikan respon http ok 200 dengan data dari select di atas
      $this->response($data,Restdata::HTTP_OK);
    } else {
        $this->notfound('Jabatan Tidak Di Temukan');
    }
  }

  function jabatan_post()
  {
    $nip = $this->post('nip',TRUE);

    $data = $this->mymodel->selectjabatanwhere($nip);

    if ($data!=FALSE) {
      //mengembalikan respon http ok 200 dengan data dari select di atas
      $this->response($data,Restdata::HTTP_OK);
    } else {
      $this->notfound('Jabatan Tidak Di Temukan');
    }
  }

}
