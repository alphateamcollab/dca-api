<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/api/Restdata.php';

class Apiadmincontroller extends Restdata{

  public function __construct(){
    parent::__construct();
    $this->load->model('mymodel');
  }

  //method untuk melakukan penambahan admin (post)
  function admin_post()
  {

    $nama = $this->post('nama');
    $kata_sandi = password_hash($this->post('kata_sandi'),PASSWORD_DEFAULT);

    $this->form_validation->set_rules('nama','Nama Pengguna','trim|max_length[50]|is_unique[t_admin.admin_nama]');
    $this->form_validation->set_rules('kata_sandi','Kata Sandi','trim|min_length[8]');

    if ($this->form_validation->run()==false) {
      $this->badreq($this->validation_errors());
    } else {
      // if ($this->mymodel->insertadmin($data)) {

      if ($this->form_validation->run()==false) {

        $this->badreq($this->validation_errors());

      } else {
        if ($this->mymodel->insertadmin($nama,$kata_sandi)) {

        $this->response(
          [
            'message'=>'Admin Berhasil Di Buat'
          ],HTTP_OK);

        } else {
          $this->response(
            [
              'message'=>'Admin Gagal Di Buat'
            ],'HTTP_BAD_REQUEST');

        }
      }
    }
  }

}
