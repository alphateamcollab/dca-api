<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/api/Restdata.php';

/**
 *  class untuk Api pendidikan, load Mymodel, cektoken di controller Restdata
 */
class Apipendidikancontroller extends Restdata
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('mymodel');
    //mengecek token pada class Restdata, di mana jika token invalid maka akan melakukan exit
    $this->cektoken();
  }

//get berdasarkan id (nip) dan tampilkan pendidikan terakhir
  function pendidikan_get()
  {
    $id = $this->get('id',TRUE);

    $data = $this->mymodel->selectpendidikanwhere($id);

    if ($data!=FALSE) {
      //mengembalikan respon http ok 200 dengan data dari select di atas
      $this->response($data,Restdata::HTTP_OK);
    } else {
        $this->notfound('Pendidikan Tidak Di Temukan');
    }
  }

//post berdasarkan id (nip) dan tampilkan riwayat pendidikan
  function pendidikan_post()
  {
    $nip = $this->post('nip',TRUE);

    $data = $this->mymodel->selectpendidikan($nip);

    if ($data!=FALSE) {
      //mengembalikan respon http ok 200 dengan data dari select di atas
      $this->response($data,Restdata::HTTP_OK);
    } else {
      $this->notfound('Pendidikan Tidak Di Temukan');
    }
  }

}
